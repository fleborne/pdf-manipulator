import os
from PyPDF2 import PdfFileReader, PdfFileWriter
from CropParams import CropParams

class PdfTransformer(object):
    def __init__(self,  input_file, output_file):
        self.setInputFile(input_file)
        self.output_file = output_file

    def setInputFile(self, input_file):
        if os.path.exists(input_file):
            self.input_file = input_file
        else:
            self.input_file = None
            print('[WARN] Input file doesn\'t exist.')

    def arrangeForKobo(self,
                       crop_params,
                       rotation=90):
        '''
        :param crop_params: Cropping parameters
        :param rotation: Optional.
                         Angle of counter clockwise rotation of the pages (must
                         be a multiple of 90).
                         Default: 90
        '''
        reader_top = PdfFileReader(self.input_file, 'r')
        reader_bottom = PdfFileReader(self.input_file, 'r')
        writer = PdfFileWriter()

        for i in range(reader_top.getNumPages()):
            page_top = reader_top.getPage(i)
            page_bottom = reader_bottom.getPage(i)

            box = reader_top.getPage(0).mediaBox
            lower_left_top, upper_right_top, lower_left_bottom, upper_right_bottom = \
                crop_params[i % len(crop_params)].computeCorners(box)

            # top half
            page_top.cropBox.setLowerLeft(lower_left_top)
            page_top.cropBox.setUpperRight(upper_right_top)
            page_top.rotateCounterClockwise(rotation)
            writer.addPage(page_top)

            # lower half
            page_bottom.cropBox.setLowerLeft(lower_left_bottom)
            page_bottom.cropBox.setUpperRight(upper_right_bottom)
            page_bottom.rotateCounterClockwise(rotation)
            writer.addPage(page_bottom)

        self.makeToC(reader_top, writer)
        self.setMetadata(reader_top, writer)
        self.savePdf(writer)

    def setMetadata(self, reader, writer):
        metadata = reader.getDocumentInfo()
        try:
            writer.addMetadata(metadata)
        except TypeError:
            print("[WARNING] Unable to set metadata")
            pass

    def makeToC(self, reader, writer, parent=None, outlines=None):
        if outlines == None: outlines = reader.getOutlines()

        for o in outlines:
            if type(o) is list:
                self.makeToC(reader, writer, new_parent, o)
            else:
                new_parent = writer.addBookmark(o['/Title'], 2 * reader.getDestinationPageNumber(o), parent)

    def savePdf(self, writer):
        with open(self.output_file, 'wb') as f:
            writer.write(f)
