import cv2, os

from PyPDF2 import PdfFileReader, PdfFileWriter
from PdfTransformer import PdfTransformer
from CropParams import CropParams

from wand.image import Image
from wand.color import Color

class Preview(object):
    def __init__(self, preview_images=list(range(10))):
        self.PREVIEW_DIR = '.previews'
        if not os.path.isdir(self.PREVIEW_DIR): os.mkdir(self.PREVIEW_DIR)

        self.generated_previews = set()
        self.preview_images = preview_images

        self.crop_params = [
            CropParams(),
            CropParams()
        ]

        self.update = True
        self.previewed_page = 0
        self.reader = None

    def onChange(self, _):
        self.update = True

    def updateCropParams(self):
        odd_or_even = self.previewed_page % 2

        self.crop_params[odd_or_even].relative_overlap       = \
            0.01 * float(cv2.getTrackbarPos('overlap'      , 'preview'))
        self.crop_params[odd_or_even].relative_margin_top    = \
            0.01 * float(cv2.getTrackbarPos('margin_top'   , 'preview'))
        self.crop_params[odd_or_even].relative_margin_right  = \
            0.01 * float(cv2.getTrackbarPos('margin_right' , 'preview'))
        self.crop_params[odd_or_even].relative_margin_bottom = \
            0.01 * float(cv2.getTrackbarPos('margin_bottom', 'preview'))
        self.crop_params[odd_or_even].relative_margin_left   = \
            0.01 * float(cv2.getTrackbarPos('margin_left'  , 'preview'))

    def generatePreview(self, page_num):
        self.generated_previews.add(page_num)

        filename = ''.join(['preview', str(page_num), '.'])
        filename_pdf = os.path.join(self.PREVIEW_DIR, filename + 'pdf')
        filename_bmp = os.path.join(self.PREVIEW_DIR, filename + 'bmp')

        writer = PdfFileWriter()
        writer.addPage(self.reader.getPage(page_num))

        with open(filename_pdf, 'wb') as f:
            writer.write(f)

        with(Image(filename=filename_pdf, background=Color('#fff'))) as image:
            image.save(filename=filename_bmp)

    def preview(self, input_file):
        self.reader = PdfFileReader(input_file)

        cv2.namedWindow('preview', cv2.WINDOW_NORMAL)
        cv2.createTrackbar('overlap'      , 'preview', 0,  50, self.onChange)
        cv2.createTrackbar('margin_top'   , 'preview', 0, 100, self.onChange)
        cv2.createTrackbar('margin_right' , 'preview', 0, 100, self.onChange)
        cv2.createTrackbar('margin_bottom', 'preview', 0, 100, self.onChange)
        cv2.createTrackbar('margin_left'  , 'preview', 0, 100, self.onChange)
        self.onChange(None)

        idx, quit, crop = 0, False, False

        while not quit:
            if self.update:
                self.updateCropParams()

                self.previewed_page = self.preview_images[idx]
                odd_or_even = self.previewed_page % 2

                cv2.setTrackbarPos('overlap',
                                   'preview',
                                   int(100 * self.crop_params[odd_or_even].relative_overlap))
                cv2.setTrackbarPos('margin_top',
                                   'preview',
                                   int(100 * self.crop_params[odd_or_even].relative_margin_top))
                cv2.setTrackbarPos('margin_right',
                                   'preview',
                                   int(100 * self.crop_params[odd_or_even].relative_margin_right))
                cv2.setTrackbarPos('margin_bottom',
                                   'preview',
                                   int(100 * self.crop_params[odd_or_even].relative_margin_bottom))
                cv2.setTrackbarPos('margin_left',
                                   'preview',
                                   int(100 * self.crop_params[odd_or_even].relative_margin_left))

                if not self.previewed_page in self.generated_previews:
                    self.generatePreview(self.previewed_page)

                filename = ''.join(['preview', str(self.previewed_page), '.bmp'])
                filename_bmp = os.path.join(self.PREVIEW_DIR, filename)

                img = cv2.imread(filename_bmp)
                height, _, _ = img.shape

                box = self.reader.getPage(0).mediaBox
                corners = self.crop_params[odd_or_even].computeCorners(box)

                lower_left_top = corners[0]
                upper_right_top = corners[1]
                lower_left_bottom = corners[2]
                upper_right_bottom = corners[3]

                cv2.rectangle(img,
                              (int(lower_left_bottom[0]),
                               height - int(lower_left_bottom[1])),
                              (int(upper_right_bottom[0]),
                               height - int(upper_right_bottom[1])),
                              (255, 0, 0),
                              2)

                cv2.rectangle(img,
                              (int(lower_left_top[0]),
                               height - int(lower_left_top[1])),
                              (int(upper_right_top[0]),
                               height - int(upper_right_top[1])),
                              (0, 0, 255),
                              2)

                cv2.imshow('preview', img)
                self.update = False

            key = cv2.waitKey(500)
            if key != -1:
                self.update = True

            if key == 27:
                quit = True
                crop = False
            elif key == 13:
                quit = True
                crop = True
            elif key in [8, ord('p')]:
                idx = (idx - 1) % len(self.preview_images)
            elif key in [ord(' '), ord('n')]:
                idx = (idx + 1) % len(self.preview_images)

        cv2.destroyAllWindows()

        return crop, self.crop_params
