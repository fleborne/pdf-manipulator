import unittest
from CropParams import CropParams

class TestCropParams(unittest.TestCase):
    def test_getAbsoluteMiddle(self):
        crop_params = CropParams()

        y_min = 0.0
        y_max = 10.0
        margin_top = 0.0
        margin_bottom = 0.0

        result = crop_params.getAbsoluteMiddle(y_min,
                                               y_max,
                                               margin_top,
                                               margin_bottom)
        self.assertEqual(result, 5.0)

        margin_top = 1.0
        result = crop_params.getAbsoluteMiddle(y_min,
                                               y_max,
                                               margin_top,
                                               margin_bottom)
        self.assertEqual(result, 4.5)

        margin_bottom = 2.0
        result = crop_params.getAbsoluteMiddle(y_min,
                                               y_max,
                                               margin_top,
                                               margin_bottom)
        self.assertEqual(result, 5.5)

    def test_getAbsoluteMargins(self):
        crop_params = CropParams()

        x_min = 0.0
        x_max = 250.0
        y_min = 0.0
        y_max = 100.0

        overlap, margin_top, margin_right, margin_bottom, margin_left = \
            crop_params.getAbsoluteMargins(x_min, x_max, y_min, y_max)

        self.assertEqual(overlap      , 0.0)
        self.assertEqual(margin_top   , 0.0)
        self.assertEqual(margin_right , 0.0)
        self.assertEqual(margin_bottom, 0.0)
        self.assertEqual(margin_left  , 0.0)

        crop_params.setRelativeMargins(relative_margin_top=0.1)
        overlap, margin_top, margin_right, margin_bottom, margin_left = \
            crop_params.getAbsoluteMargins(x_min, x_max, y_min, y_max)
        self.assertEqual(margin_top, 10.0)

        crop_params.setRelativeMargins(relative_margin_left=0.15)
        overlap, margin_top, margin_right, margin_bottom, margin_left = \
            crop_params.getAbsoluteMargins(x_min, x_max, y_min, y_max)
        self.assertEqual(margin_left, 37.5)

if __name__ == "__main__":
    unittest.main()
