class CropParams(object):
    def __init__(self,
                 relative_margin_top=0,
                 relative_margin_right=0,
                 relative_margin_bottom=0,
                 relative_margin_left=0,
                 relative_overlap=0):

        self.setRelativeMargins(relative_margin_top,
                                relative_margin_right,
                                relative_margin_bottom,
                                relative_margin_left,
                                relative_overlap)

    def __repr__(self):
        return ''.join(["- overlap: {}\n".format(self.relative_overlap),
                        "- margin_top: {}\n".format(self.relative_margin_top),
                        "- margin_right: {}\n".format(self.relative_margin_right),
                        "- margin_bottom: {}\n".format(self.relative_margin_bottom),
                        "- margin_left: {}\n".format(self.relative_margin_left)])

    def setRelativeMargins(self,
                           relative_margin_top=0,
                           relative_margin_right=0,
                           relative_margin_bottom=0,
                           relative_margin_left=0,
                           relative_overlap=0):

        self.relative_margin_top = relative_margin_top
        self.relative_margin_right = relative_margin_right
        self.relative_margin_bottom = relative_margin_bottom
        self.relative_margin_left = relative_margin_left
        self.relative_overlap = relative_overlap

    def getAbsoluteMiddle(self, y_min, y_max, margin_top, margin_bottom):
        return 0.5 * (y_max - margin_top - y_min + margin_bottom)

    def getAbsoluteMargins(self, x_min, x_max, y_min, y_max):
        margin_top    = (y_max - y_min) * self.relative_margin_top
        margin_right  = (x_max - x_min) * self.relative_margin_right
        margin_bottom = (y_max - y_min) * self.relative_margin_bottom
        margin_left   = (x_max - x_min) * self.relative_margin_left
        overlap       = (y_max - y_min) * self.relative_overlap

        return overlap, margin_top, margin_right, margin_bottom, margin_left

    def computeCorners(self, box):
        x_min, y_min, x_max, y_max = box
        x_min, y_min, x_max, y_max = float(x_min), float(y_min), float(x_max), float(y_max)

        overlap, margin_top, margin_right, margin_bottom, margin_left = \
            self.getAbsoluteMargins(x_min, x_max, y_min, y_max)

        middle = self.getAbsoluteMiddle(y_min, y_max, margin_top, margin_bottom)

        lower_left_top = (margin_left, middle - overlap)
        upper_right_top = (x_max - margin_right, y_max - margin_top)

        lower_left_bottom = (margin_left, margin_bottom)
        upper_right_bottom = (x_max - margin_right, middle + overlap)

        return lower_left_top, upper_right_top, lower_left_bottom, upper_right_bottom
