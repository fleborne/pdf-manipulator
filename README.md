# PDF Manipulator

Tested with Python 3.9

## Dependencies

* PyPDF2
* OpenCV (@todo: find a lighter way to handle the GUI)
* Wand

Install with

```
python -m pip install -r requirements.txt
```

## Usage

### Rearrange for e-reader

Crop and rearrange the pages of the input file to fit nicely on an e-reader.

```
python main.py <input_file.pdf> <output_file.pdf>
```

Running this will first open a GUI that lets you set the margins and overlap
used for cropping the original PDF file. You can control the GUI with the
following keys:

* `Space` or `N`: preview next page
* `Delete` or `P`: preview previous page
* `Enter`: quit and generate cropped PDF
* `Escape`: quit and do nothing
