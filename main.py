import sys

from PdfTransformer import PdfTransformer
from Preview import Preview

if __name__ == '__main__':
    if len(sys.argv) == 2:
        input_file = sys.argv[1]
        output_file = 'output.pdf'
    elif len(sys.argv) == 3:
        input_file = sys.argv[1]
        output_file = sys.argv[2]
    else:
        print('Usage: python {} inputfile [outputfile=output.pdf]'.format(sys.argv[0]))
        exit(-1)

    preview = Preview(preview_images=list(range(10, 20)))
    crop, crop_params = preview.preview(input_file)

    if crop:
        pt = PdfTransformer(input_file, output_file)
        pt.arrangeForKobo(crop_params)
